package com.carlos.likesservice.DTO;

import com.carlos.likesservice.model.Like;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LikeDto {

  private Long id;
  private String name;
  private UserDto user;

  public Like toEntity() {

    return new Like(id, name, user != null ? user.getId() : null);

  }

  public static LikeDto fromEntity(Like like) {
    return new LikeDto(like.getId(), like.getName(), null);
  }

}
