package com.carlos.likesservice.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

  private Long id;

  private String firstName;

  private String lastName;

  private Integer idNumber;

  private String email;

  private Integer telephone;

}