package com.carlos.likesservice.controller;

import com.carlos.likesservice.DTO.LikeDto;
import com.carlos.likesservice.DTO.ResponseDto;
import com.carlos.likesservice.enums.StatusType;
import com.carlos.likesservice.exception.LogicException;
import com.carlos.likesservice.service.LikeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/likes")
@Slf4j
public class LikeController {

  @Autowired
  private LikeService likeService;

  @PostMapping
  public ResponseEntity<ResponseDto> createLike(@RequestBody LikeDto likeDto) {


    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("LIKE_CREATED", LikeDto.fromEntity(this.likeService.createLike(likeDto.toEntity())), StatusType.SUCCESS), HttpStatus.OK);

    } catch (LogicException le){

      log.error(le.getMessage());
      return new ResponseEntity<ResponseDto>(new ResponseDto(le.getMessage(), StatusType.FAILURE), HttpStatus.BAD_REQUEST);

    } catch (Exception e){
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping
  public ResponseEntity<ResponseDto> editLike(@RequestBody LikeDto likeDto) {

    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("LIKE_CREATED", LikeDto.fromEntity(this.likeService.editLike(likeDto.toEntity())), StatusType.SUCCESS), HttpStatus.OK);

    } catch (LogicException le){

      log.error(le.getMessage());
      return new ResponseEntity<ResponseDto>(new ResponseDto(le.getMessage(), StatusType.FAILURE), HttpStatus.BAD_REQUEST);

    } catch (Exception e){
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/{userId}")
  public ResponseEntity<ResponseDto> getLikesByUser(@PathVariable Long userId) {


    try {
      return new ResponseEntity<ResponseDto>(new ResponseDto("LIKES_LIST", this.likeService.getAllLikesByUser(userId).stream().map(i-> LikeDto.fromEntity(i)).collect(Collectors.toList()), StatusType.SUCCESS), HttpStatus.OK);

    } catch (Exception e){
      log.error(e.getMessage());
      return new ResponseEntity<ResponseDto>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
