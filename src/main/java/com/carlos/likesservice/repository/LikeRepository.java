package com.carlos.likesservice.repository;

import com.carlos.likesservice.model.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeRepository extends JpaRepository<Like, Long> {

  List<Like> findAllByUserId(Long userId);

  Like findLikeById(Long likeId);

}
