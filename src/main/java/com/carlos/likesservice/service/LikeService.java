package com.carlos.likesservice.service;
import com.carlos.likesservice.DTO.UserDto;
import com.carlos.likesservice.DTO.UserResponseDto;
import com.carlos.likesservice.exception.LogicException;
import com.carlos.likesservice.model.Like;
import com.carlos.likesservice.repository.LikeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class LikeService {

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private LikeRepository likeRepository;

  public Like createLike(Like like) throws LogicException {

    like.setId(null);

    UserDto user = restTemplate.getForObject("http://localhost:12122/users/user/" + like.getUserId(), UserResponseDto.class).getBody();

    if (like.getName() == null || like.getName().isEmpty()) {
      throw new LogicException("LIKE_NAME_CAN_NOT_BE_VOID");
    }

    like.setUserId(user.getId());

    return likeRepository.save(like);
  }

  public Like editLike(Like like) throws LogicException {

    if (like.getName() == null || like.getName().isEmpty()) {
      throw new LogicException("LIKE_NAME_CAN_NOT_BE_VOID");
    }

    Like savedLike = likeRepository.findLikeById(like.getId());
    savedLike.setName(like.getName());

    return likeRepository.save(savedLike);
  }

  public List<Like> getAllLikesByUser(Long userId) {

    return likeRepository.findAllByUserId(userId);

  }
}


